export interface Movies {
  id: number;
  name: string;
  category: string;
  rating: number;
  categoryId: number;
}
export interface Category {
  id: number;
  category: string;
}
export interface MovieGrouped {
  counted: number;
  rating: number;
}
