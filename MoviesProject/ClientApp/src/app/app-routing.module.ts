import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { MaincomponentComponent } from './components/maincomponent/maincomponent.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { MoviesGroupedComponent } from './components/moviesgrouped/moviesgrouped.component';
const routes: Routes = [
  { path: '', component: MaincomponentComponent , pathMatch: 'full' },
  { path: 'dashboard', component: MaincomponentComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'moviesgrouped', component: MoviesGroupedComponent },
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
