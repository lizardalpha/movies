import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MatDialogModule,MatTabsModule,MatProgressBarModule,MatTableModule,MatButtonModule, MatCheckboxModule, MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule,MatFormFieldModule,MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { AppRoutingModule }     from './app-routing.module';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';


import { MaincomponentComponent } from './components/maincomponent/maincomponent.component';
import { MoviesComponent } from './components/moviescomponent/movies.component';
import { AddMovieComponent } from './components/addmoviecomponent/addmovie.component';
import { AddCategoryComponent } from './components/addcategorycomponent/addcategory.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { MoviesGroupedComponent } from './components/moviesgrouped/moviesgrouped.component';
import { MovieService } from './services/movieservice.service';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    MaincomponentComponent,
    MoviesComponent,
    AddMovieComponent,
    CategoriesComponent,
    AddCategoryComponent,
    MoviesGroupedComponent,

  ],
  entryComponents:[],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatDialogModule,MatTabsModule,MatProgressBarModule,MatTableModule, MatInputModule , MatFormFieldModule, MatButtonModule,MatCheckboxModule, BrowserAnimationsModule, MatGridListModule, MatCardModule, MatMenuModule, MatIconModule, LayoutModule, MatToolbarModule, MatSidenavModule, MatListModule, AppRoutingModule,
    AppRoutingModule,
    RouterModule.forRoot([
     
      { path: 'movie/edit/:id', component: AddMovieComponent },
      { path: 'category/edit/:id', component: AddCategoryComponent },
     
    ]),
    ReactiveFormsModule,
  ],
  providers: [MovieService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
