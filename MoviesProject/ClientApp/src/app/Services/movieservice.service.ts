import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Movies } from '../Models/movies.model'
import { Category } from '../Models/movies.model'
import { MovieGrouped } from '../../Models/movies.model';

@Injectable()
export class MovieService {
  myAppUrl: string = "";
  currentMove: Movies;
  constructor(private _http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.myAppUrl = baseUrl;
  }
  filterMovies(filterValue) {
    return this._http.get<Movies[]>('/api/Movies/GetAllMovies/0/' + filterValue).catch(this.errorHandler);

     
  
  }

  getMovies() {
    //return null;
    return this._http.get<Movies[]>(this.myAppUrl + 'api/Movies/GetAllMovies')
      .catch(this.errorHandler);
  }
  getMoviesGrouped() {
    //return null;
    return this._http.get<MovieGrouped[]>(this.myAppUrl + 'api/Movies/getAllMoviesGrouped')
      .catch(this.errorHandler);
  }
  getMovie(id) {
    return this._http.get<Movies[]>(this.myAppUrl + 'api/Movies/GetAllMovies/'+ id)
      .catch(this.errorHandler);
  }

  getCategory(id) {
    return this._http.get<Category[]>(this.myAppUrl + 'api/Movies/GetAllCategories/' + id)
      .catch(this.errorHandler);
  }

  insertupdateMovie(movie) {
    console.log(movie, 'is what is being sent');
    return this._http.post<string>(this.myAppUrl + 'api/Movies/insertUpdateMovie/',movie)
      .catch(this.errorHandler);
  }

  insertUpdateCategory(category) {
    return this._http.post<string>(this.myAppUrl + 'api/Movies/insertUpdateCategory/', category)
      .catch(this.errorHandler);
  }

  deleteMovie(movie) {
    console.log(movie);
    return this._http.post<string>(this.myAppUrl + 'api/Movies/deleteMovie/', movie)
      .catch(this.errorHandler);
  }

  getCategories() {
    return this._http.get<Category[]>(this.myAppUrl + 'api/Movies/GetAllCategories')
      .catch(this.errorHandler);
  }
  errorHandler(error: Response) {
    console.log(error);
    return Observable.throw(error);
  }
}

