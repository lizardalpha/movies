import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { MovieService } from '../../Services/movieservice.service'
import { Category } from '../../Models/movies.model';
@Component({
  selector: 'app-categories-component',
  templateUrl: './categories.component.html',
  providers: [MovieService],
})
export class CategoriesComponent {
  categories: Category[];
  constructor(public http: HttpClient, private _router: Router, private _movieService: MovieService) {
    this.getCategories();
  }
  getCategories() {
    this._movieService.getCategories().subscribe(
      
      data => this.categories = data
    )
  }
  ngOnInit() {
    this.getCategories();
  }
 
  
}
