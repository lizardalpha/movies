import { Component, Input, OnInit, Injectable} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { MovieService } from '../../Services/movieservice.service'
import { Category } from '../../Models/movies.model';

@Component({
  selector: 'app-addcategory-component',
  templateUrl: './addcategory.component.html',
  providers: [MovieService],
})

export class AddCategoryComponent implements OnInit {
  id: number;
  public currentMovie: Category;
  title: string = 'Add Category';
  categoryForm: FormGroup;
  errorMessage: any;
  submitted = false;
  returnMessage: string;
  myresponse: any;
  
  //title = "Add Movies";
  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
    private _movieService: MovieService, private _router: Router) {
    this.categoryForm = this._fb.group({
      id: [0, [Validators.required]],
      category: ['', [Validators.required]],
      

    })
    if (this._avRoute.snapshot.params["id"]) {
     
      this.id = this._avRoute.snapshot.params["id"];
      if (this.id > 0) {
        this.title = "Edit Category ";
        _movieService.getCategory(this.id).subscribe(resp => this.categoryForm.setValue({ 'id': this.id, 'category': resp[0].category})
          , error => this.errorMessage = error);
        //_movieService.getMovie(this.id).subscribe(resp => this.myresponse = resp,
        //  error => this.errorMessage = error);
       
      }
    }
   

  }
  goBack() {
    this._router.navigate(['/categories']);
  }
  ngOnInit() {
    if (this.id > 0) {
      //this.title = "Edit";
      //this._employeeService.getEmployeeById(this.id)
      //  .subscribe(resp => this.employeeForm.setValue(resp)
      //    , error => this.errorMessage = error);
    }
  }
  get f() { return this.categoryForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.categoryForm.invalid) {
      return;
    }
    else {
     
      this._movieService.insertUpdateCategory(this.categoryForm.value).subscribe(resp => this.returnMessage = resp
        , error => this.errorMessage = error);
      this.goBack();
    }

   
  }

}

