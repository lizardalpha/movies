import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';
import { MovieGrouped } from '../../Models/movies.model';
import { MovieService } from '../../Services/movieservice.service'
@Component({
  selector: 'app-moviesgrouped-component',
  templateUrl: './moviesgrouped.component.html',
  //template: '<app-addmovie-component [currentMovie]="currentMovie"></app-addmovie-component>',
  providers: [MovieService],
})
export class MoviesGroupedComponent {
  public moviesGrouped: MovieGrouped[];
  errorMessage: any;
  submitted = false;
  returnMessage: string;
  myresponse: any;
  
  constructor(public http: HttpClient, private _router: Router, private _movieService: MovieService) {
    this.getMoviesGrouped();
  }
  
  ngOnInit() {
    this.getMoviesGrouped();
  }
  
  getMoviesGrouped() {
    this._movieService.getMoviesGrouped().subscribe(
      data => this.moviesGrouped = data
    )
  }
  
}

