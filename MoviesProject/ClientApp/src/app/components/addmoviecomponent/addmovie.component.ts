import { Component, Input, OnInit, Injectable} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { MovieService } from '../../Services/movieservice.service'
import { Movies } from '../../Models/movies.model';
import { Category } from '../../Models/movies.model';

@Component({
  selector: 'app-addmovie-component',
  templateUrl: './addmovie.component.html',
  providers: [MovieService],
})

export class AddMovieComponent implements OnInit {
  id: number;
  public currentMovie: Movies;
  categories: Category[];
  title: string;
  moviesForm: FormGroup;
  errorMessage: any;
  submitted = false;
  returnMessage: string;
  myresponse: any;
  
  //title = "Add Movies";
  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
    private _movieService: MovieService, private _router: Router) {
    this.moviesForm = this._fb.group({
      id: [0, [Validators.required]],
      name: ['', [Validators.required]],
      //category: ['', [Validators.required]],
      rating: ['', [Validators.max(10), Validators.min(0), Validators.required]],
      categoryId: ['', [Validators.required]],

    })
    if (this._avRoute.snapshot.params["id"]) {
     
      this.id = this._avRoute.snapshot.params["id"];
      if (this.id > 0) {
        this.title = "Edit Movie ";
        _movieService.getMovie(this.id).subscribe(resp => this.moviesForm.setValue({ 'id': this.id,  'name': resp[0].name, 'rating': resp[0].rating, 'categoryId':resp[0].categoryId})
          , error => this.errorMessage = error);
        //_movieService.getMovie(this.id).subscribe(resp => this.myresponse = resp,
        //  error => this.errorMessage = error);
       
      }
    }
   
    this.getCategories();
  }
  goBack() {
    this._router.navigate(['/']);
  }
  getCategories() {
    this._movieService.getCategories().subscribe(

      data => this.categories = data
    )
  }
  ngOnInit() {
    if (this.id > 0) {
      //this.title = "Edit";
      //this._employeeService.getEmployeeById(this.id)
      //  .subscribe(resp => this.employeeForm.setValue(resp)
      //    , error => this.errorMessage = error);
    }
  }
  get f() { return this.moviesForm.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.moviesForm.invalid) {
      return;
    }
    else {
     
      this._movieService.insertupdateMovie(this.moviesForm.value).subscribe(resp => this.returnMessage = resp
        , error => this.errorMessage = error);
      this.goBack();
    }

   
  }

}

