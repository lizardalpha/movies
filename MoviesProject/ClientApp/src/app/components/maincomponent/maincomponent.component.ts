import { Component } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointState, BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-maincomponent',
  templateUrl: './maincomponent.component.html',
  styleUrls: ['./maincomponent.component.css']
})
export class MaincomponentComponent {
  /** Based on the screen size, switch from standard to one column per row */
  //movies = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
  //  map(({ matches }) => {
  //    if (matches) {
  //      return [
  //        { title: 'Movie 1', cols: 1, rows: 1, description : 'This is a movie' },
  //        { title: 'Movie 2', cols: 1, rows: 1, description: 'This is a movie' },
  //        { title: 'Card 3', cols: 1, rows: 1, description: 'This is a movie' },
  //        { title: 'Card 4', cols: 1, rows: 1, description: 'This is a movie' }
  //      ];
  //    }

  //    return [
  //      { title: 'Movie 1', cols: 2, rows: 1, description: 'This is a movie' },
  //      { title: 'Movie 2', cols: 1, rows: 1, description: 'This is a movie' },
  //      { title: 'Card 3', cols: 1, rows: 2, description: 'This is a movie' },
  //      { title: 'Card 4', cols: 1, rows: 1, description: 'This is a movie' }
  //    ];
  //  })
   
  //);
  //public filterMovies() {
  //  alert('help');
  //}
  
 
  //constructor(private breakpointObserver: BreakpointObserver) {}
  public filterMovies() {
    alert('help');
  }
}
