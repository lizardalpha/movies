import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { Router, ActivatedRoute } from '@angular/router';

import { MovieService } from '../../Services/movieservice.service'
@Component({
  selector: 'app-movies-component',
  templateUrl: './movies.component.html',
  //template: '<app-addmovie-component [currentMovie]="currentMovie"></app-addmovie-component>',
  providers: [MovieService],
})
export class MoviesComponent {
  public currentCount = 0;
  public filterValue = "";
  public movies: Movies[];
 
  public currentMovie = "";
  errorMessage: any;
  submitted = false;
  returnMessage: string;
  myresponse: any;

  public addMovie: boolean;

 
  constructor(public http: HttpClient, private _router: Router, private _movieService: MovieService) {
    this.getMovies();
  }
  getMovies() {
    this._movieService.getMovies().subscribe(
      data => this.movies = data
    )
  }
  edit(movie) {
   
    this.addMovie = !this.addMovie;
    this.getMovies();
  }
  ngOnInit() {
    this.getMovies();
  }
  delete(movie) {
    this._movieService.deleteMovie(movie).subscribe(resp => this.myresponse = resp
      , error => this.errorMessage = error);
    this.getMovies();
  }
  
  public filterMovies() {

    this._movieService.filterMovies(this.filterValue).subscribe(
      data => this.movies = data
    )
    
  }


}
interface Movies {
  id: number;
  name: string;
  category: string;
  rating: number;
  categoryId: number;
}
