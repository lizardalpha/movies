﻿using System;
using DataAccess.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Dapper;
using System.Data.SqlClient;
using System.Web.Http;


namespace MoviesProject.Controllers.api
{
    [Route("api/Movies")]
    [ApiController]
    public class MoviesController : ControllerBase
    {

        IMovieService _movieService;

        //public MoviesController()
        //{

        //}
        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }
        [HttpGet("[action]/{id?}/{filter?}/")]
        public async Task<IActionResult> GetAllMovies(int id =0, string filter = "")
        {
            var resultData = await _movieService.getAllMovies(id,filter);
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllMoviesGrouped()
        {
            var resultData = await _movieService.getAllMoviesGrouped();
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }

        [HttpGet("[action]/{id?}")]
        public async Task<IActionResult> GetAllCategories(int id = 0, string filter = "")
        {
           var resultData = await _movieService.getAllCategories(id);
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }


        [HttpPost("[action]")]
        public async Task<IActionResult> insertUpdateMovie(DataAccess.Entities.Movie movie)
        {
            var resultData = await _movieService.insertUpdateMovie(movie);
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> insertUpdateCategory(DataAccess.Entities.Category category)
        {
            var resultData = await _movieService.insertUpdateCategory(category);
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> deleteMovie(DataAccess.Entities.Movie movie)
        {
            var resultData = await _movieService.deleteMovie(movie.id);
            if (resultData == null)
            {
                return NotFound();
            }
            return Ok(resultData);
        }

        [HttpGet("[action]/{filterValue?}")]
        public  IEnumerable<Movie> GetMovies(string filterValue = "")
        {
         

            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\vstadenl\source\repos\MoviesProject\MoviesProject\Data\MoviesDB.mdf;Integrated Security=True");
            connection.Open();


            List<Movie> movies = new List<Movie>();
            Movie movie = new Movie() { id = 1, name = "The Pretender", category = "Action", rating = 1 };
            movies.Add(movie);

            movie = new Movie() { id = 2, name = "Castaway", category = "Action", rating = 1 };
            movies.Add(movie);

            movie = new Movie() { id = 3, name = "The Notebook", category = "Romance", rating = 1 };
            movies.Add(movie);

            movie = new Movie() { id = 4, name = "Pretty Woman", category = "Romance", rating = 1 };
            movies.Add(movie);

            movie = new Movie() { id = 5, name = "Shrek", category = "Comedy", rating = 1 };
            movies.Add(movie);

            movie = new Movie() { id = 6, name = "Dumb and Dumber", category = "Comedy", rating = 1 };
            movies.Add(movie);

            if (filterValue != "")
            {
                List<Movie> filteredMovie = movies.Where(x => x.name.ToLower().Contains(filterValue.ToLower())).ToList();
                return filteredMovie;
            }

            

            return movies.Where(x => x.name.Contains(filterValue)).ToList();



        }

        [HttpGet("[action]/{id?}")]
        public Movie GetMovie(int id)
        {
            Movie movie = new Movie() { id = 1, name = "The Pretender", category = "Action", rating = 1 };
            return movie;
        }
    }
    public class Movie
    {
        public int id;
        public string name;
        public string category;
        public int rating;

    }
}