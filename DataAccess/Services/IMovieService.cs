﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> getAllMovies(int id, string filter);
        Task<IEnumerable<MovieGrouped>> getAllMoviesGrouped();
        Task<string> insertUpdateMovie(Movie movie);

        Task<IEnumerable<Category>> getAllCategories(int id);

        Task<string> insertUpdateCategory(Category category);
        Task<string> deleteMovie(int id);
    }
}
