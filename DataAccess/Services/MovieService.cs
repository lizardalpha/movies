﻿using DataAccess.Entities;
using DataAccess.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Services
{
    public class MovieService : IMovieService
    {
        IUnitOfWork _unitOfWork;
        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Category>> getAllCategories(int id)
        {
            return await _unitOfWork.MovieRepository.getAllCategories(id);
        }

        public async Task<IEnumerable<Movie>> getAllMovies(int id = 0, string filter = "")
        {
            return await _unitOfWork.MovieRepository.getAllMovies(id,filter);
        }

        public async Task<string> insertUpdateMovie(Movie movie)
        {
            return await _unitOfWork.MovieRepository.insertUpdateMovie(movie);
        }
        public async Task<string> insertUpdateCategory(Category category)
        {
            return await _unitOfWork.MovieRepository.insertUpdateCategory(category);
        }

        public async Task<string> deleteMovie(int id)
        {
            return await _unitOfWork.MovieRepository.deleteMovie(id);
        }

        public async Task<IEnumerable<MovieGrouped>> getAllMoviesGrouped()
        {
            return await _unitOfWork.MovieRepository.getAllMoviesGrouped();
        }
    }
}