﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class Movie
    {
        public int id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public int rating { get; set; }
        public int categoryId { get; set; }
    }
    public class MovieGrouped
    {
        public int counted { get; set; }
        public int rating { get; set; }
    }
}
