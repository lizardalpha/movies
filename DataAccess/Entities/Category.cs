﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Entities
{
    public class Category
    {
        public int id { get; set; }
        public string category { get; set; }
    }
}
