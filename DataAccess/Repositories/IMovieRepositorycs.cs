﻿using DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        Task<IEnumerable<Movie>> getAllMovies(int id, string filter);
        Task<IEnumerable<Category>> getAllCategories(int id);
        Task<string> insertUpdateMovie(Movie movie);

        Task<string> insertUpdateCategory(Category category);
        Task<string> deleteMovie(int id);
        Task<IEnumerable<MovieGrouped>> getAllMoviesGrouped();
    }
}
