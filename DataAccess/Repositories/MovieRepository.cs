﻿using Dapper;
using DataAccess.Entities;
using DataAccess.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Repositories
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        IConnectionFactory _connectionFactory;

        public MovieRepository(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<IEnumerable<Movie>> getAllMovies(int id = 0, string filter = "")
        {
            string queryString = "select m.id,m.name,m.rating,c.category, c.id as categoryId from movies m inner join categories c on m.categoryid = c.id";
            if(id != 0)
            {
                queryString += " where m.id = '" + id + "'";
            }
            else if(id == 0 && filter != "")
            {
                queryString += " where m.name like '%" + filter + "%' or c.category like '%" + filter + "%'";            }
           
            var param = new DynamicParameters();
           
            var list = await SqlMapper.QueryAsync<Movie>(_connectionFactory.GetConnection, queryString, param, commandType: CommandType.Text);
            return list;
        }
        public async Task<IEnumerable<MovieGrouped>> getAllMoviesGrouped()
        {
            string queryString = "getByRatingGrouped";
            var param = new DynamicParameters();
            var list = await SqlMapper.QueryAsync<MovieGrouped>(_connectionFactory.GetConnection, queryString, param, commandType: CommandType.StoredProcedure);
            return list;
        }
        public async Task<IEnumerable<Category>> getAllCategories(int id = 0)
        {
            
            string queryString = "select * from categories ";
            if (id != 0)
            {
                queryString += " where id = '" + id + "'";
            }

            var param = new DynamicParameters();

            var list = await SqlMapper.QueryAsync<Category>(_connectionFactory.GetConnection, queryString, param, commandType: CommandType.Text);
            return list;
        }

        public async Task<string> insertUpdateMovie(Movie movie)
        {
            var query = "addEditMovie";
            var param = new DynamicParameters();
            param.Add("@id", movie.id);
            param.Add("@name", movie.name);
            param.Add("@category", movie.category);
            param.Add("@rating", movie.rating);
            param.Add("@categoryId", movie.categoryId);

            try
            {
                var message = await SqlMapper.QueryAsync<string>(_connectionFactory.GetConnection, query, param, commandType: CommandType.StoredProcedure);
                return message.ToString();
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
          
        }

        public async Task<string> deleteMovie(int id)
        {
            var query = "delete from Movies where id ='" + id + "'";
            var param = new DynamicParameters();
           

            try
            {
                var message = await SqlMapper.QueryAsync<string>(_connectionFactory.GetConnection, query, param, commandType: CommandType.Text);
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public async Task<string> insertUpdateCategory(Category category)
        {
            var query = "addEditCategory";
            var param = new DynamicParameters();
            param.Add("@id", category.id);
            param.Add("@category", category.category);
           
            try
            {
                var message = await SqlMapper.QueryAsync<string>(_connectionFactory.GetConnection, query, param, commandType: CommandType.StoredProcedure);
                return message.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
    }
}
